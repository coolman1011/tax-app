<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Model\Token;

class ApiTest extends TestCase
{
    protected $getReceipt = "/api/v1/receipt";

    public function testCATaxRate(){
        $data = [
            [
                "productName" => "book",
                "price" => "17.99",
                "quantity" => "1",
                "location" => "CA"
            ],
            [
                "productName" => "potato chips",
                "price" => "3.99",
                "quantity" => "1",
                "location" => "CA"
            ]
        ];

        $response = $this->json('POST', $this->getReceipt, $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'SUCCESS',
                "data" => []
            ]);

        $data = json_decode($response->getContent(), true);
        $data = $data["data"];
        $this->assertArrayHasKey('items', $data);
            $this->assertEquals(2,count($data['items']));
        $this->assertArrayHasKey('subTotal', $data);
            $this->assertEquals('21.98', $data['subTotal']);
        $this->assertArrayHasKey('tax', $data);
            $this->assertEquals('1.80', $data['tax']);
        $this->assertArrayHasKey('total', $data);
            $this->assertEquals('23.78', $data['total']);
    }

    public function testNYTaxRate(){
        $data = [
            [
                "productName" => "book",
                "price" => "17.99",
                "quantity" => "1",
                "location" => "NY"
            ],
            [
                "productName" => "pencil",
                "price" => "2.99",
                "quantity" => "3",
                "location" => "NY"
            ]
        ];

        $response = $this->json('POST', $this->getReceipt, $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'SUCCESS',
                "data" => []
            ]);

        $data = json_decode($response->getContent(), true);
        $data = $data["data"];
        $this->assertArrayHasKey('items', $data);
            $this->assertEquals(2,count($data['items']));
        $this->assertArrayHasKey('subTotal', $data);
            $this->assertEquals('26.96', $data['subTotal']);
        $this->assertArrayHasKey('tax', $data);
            $this->assertEquals('2.40', $data['tax']);
        $this->assertArrayHasKey('total', $data);
            $this->assertEquals('29.36', $data['total']);
    }

    public function testNYTaxRate2(){
        $data = [
            [
                "productName" => "pencil",
                "price" => "2.99",
                "quantity" => "2",
                "location" => "NY"
            ],
            [
                "productName" => "shirt",
                "price" => "29.99",
                "quantity" => "1",
                "location" => "NY"
            ]
        ];

        $response = $this->json('POST', $this->getReceipt, $data);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'SUCCESS',
                "data" => []
            ]);

        $data = json_decode($response->getContent(), true);
        $data = $data["data"];
        $this->assertArrayHasKey('items', $data);
            $this->assertEquals(2,count($data['items']));
        $this->assertArrayHasKey('subTotal', $data);
            $this->assertEquals('35.97', $data['subTotal']);
        $this->assertArrayHasKey('tax', $data);
            $this->assertEquals('0.55', $data['tax']);
        $this->assertArrayHasKey('total', $data);
            $this->assertEquals('36.52', $data['total']);
    }

    public function testAssertEmptyRequest(){
        $data = [];

        $response = $this->json('POST', $this->getReceipt, $data);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'REQUEST__CAN_NOT_BE_EMPTY',
                "data" => []
            ]);
    }

    public function testAssertEmptyItems(){
        $data = [
            [
                "productName" => "pencil",
                "price" => "2.99",
                "quantity" => "2",
                "location" => "NY"
            ],
            []
        ];

        $response = $this->json('POST', $this->getReceipt, $data);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'ITEM__CAN_NOT_BE_EMPTY',
                "data" => []
            ]);
    }

    public function testAssertMissingItemInformation(){
        $data = [
            [
                "productName" => "pencil",
                "price" => "2.99",
                "quantity" => "2",
                "location" => "NY"
            ],
            [
                "productName" => "shirt",
                "quantity" => "1",
                "location" => "NY"
            ]
        ];

        $response = $this->json('POST', $this->getReceipt, $data);
        $response
            ->assertStatus(400)
            ->assertJson([
                'status' => 'ITEM__MISSING_INFORMATIONS',
                "data" => []
            ]);
    }
}
