## Docker
```
docker run coolman1011/tax-app:latest
```
Host will be 127.0.0.1:8080

---

## API
> begin and end location
> 
> endpoint: `POST` /api/v1/receipt 
> 
Request Sample:
```json
[
    {
        "productName" : "book",
        "price" : "17.99",
        "quantity" : "1",
        "location" : "CA"
    },
    {
        "productName" : "potato chips",
        "price" : "3.99",
        "quantity" : "1",
        "location" : "CA"
    }
]
```
Response Sample:
```json
{
    "status": "SUCCESS",
    "data": {
        "items": [
            {
                "item": "book",
                "price": "17.99"
            },
            {
                "item": "potato chips",
                "price": "3.99"
            }
        ],
        "subTotal": "21.98",
        "tax": "1.80",
        "total": "23.78"
    },
    "message": "Success"
}
```
---
# Variables
## Avaliable Product Category
1. food
    1. apple
    2. banana
    3. potato chips
2. cloth
    1. suit
    2. shirt

## Locations
1. CA
    1. rate: 9.75%
    2. exempt : food
2. NY
    1. rate: 8.75%
    2. exempt: food, cloth
3. HK
    1. rate: 2.125%
    2. exempty: cloth