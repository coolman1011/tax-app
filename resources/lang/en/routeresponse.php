<?php

return [
    'SUCCESS' => [
        'status_code' => "200",
        'message' => "Success"
    ],
    'GENERAL_SAVE_ERROR' => [
        'status_code' => "500",
        'message' => "Save Error Occur"
    ],
    'UNKNOWN_ERROR' => [
        'status_code' => "500",
        'message' => "Unknown error occur, please contact system admin"
    ],
    'REQUEST__CAN_NOT_BE_EMPTY' => [
        'status_code' => "400",
        'message' => "Please make sure you pass a json with valid items"
    ],
    'REQUEST__DATA_IS_NOT_JSON' => [
        'status_code' => "400",
        'message' => "Please make sure you pass a json"
    ],
    'ITEM__CAN_NOT_BE_EMPTY' => [
        'status_code' => "400",
        'message' => "Please make sure your item is not empty"
    ],
    'ITEM__MISSING_INFORMATIONS' => [
        'status_code' => "400",
        'message' => "Please make sure you have included all required information"
    ],
    'ITEM__LOCATION_NOT_FOUND' => [
        'status_code' => "400",
        'message' => "Please make sure you have included all required information"
    ],
    'ITEM__PRICE_CAN_NOT_BE_ZERO' => [
        'status_code' => "400",
        'message' => "Please check if you have input the correct unit price"
    ],
    'ITEM__QUANTITY_CAN_NOT_BE_ZERO' => [
        'status_code' => "400",
        'message' => "Please check if you have input the correct quantity"
    ]
];