<?php
namespace App\Traits;

use App\Exceptions\ApiException;

use App;

trait JSONParser{
    private function isJSON($string){
        if(is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE)){
            return true;
        } else {
            throw new ApiException('REQUEST__DATA_IS_NOT_JSON');
        }
    }
}



?>