<?php
namespace App\Traits;

use App;

trait JSONResponse{
    /**
     * A Custom Response Functin which can be used accross different class
     * 
     * @param string $type
     * @param array $data
     * @param string $message
     */
    function apiResponse($type = 'UNKNOWN_ERROR', $data = [], $message = ''){
        App::setLocale('en');
        $status_code = 200;
        $response = [
            'status' => 'UNKNOWN_ERROR',
            'data' => [],
            'message' => "",
        ];
        $translation_prefix = 'routeresponse.'.$type;

        $status_code = $translation_prefix.".status_code";          // routeresponse.UNKNOWN_ERROR.status_code
        $status_code = trans($status_code);

        if(!is_array($data)){
            $data = [];
        }

        if($message == ''){
            $message = $translation_prefix.".message";              // routeresponse.UNKNOWN_ERROR.message
            $message = trans($message);          
        }

        $response = [
            'status' => $type,
            'data' => $data,
            'message' => $message,
        ];
        return response()->json($response, (int)$status_code);
    }
}
?>