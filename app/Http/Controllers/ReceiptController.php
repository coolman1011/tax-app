<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tax;

use App\Traits\JSONResponse;
use App\Traits\JSONParser;

class ReceiptController extends Controller
{
    use JSONResponse;
    use JSONParser;
    /**
     * Routing to generate receipt with tax rate
     * 
     * endpoint: `POST` /api/v1/receipt
     * 
     * ```json
     * [
	 *   {
	 *     "productName" : "apple",
	 *     "price" : "123",
	 *     "quantity" : "1",
	 *     "location" : "NY"
	 *   }
     * ]
     * ```
     */
    public function receiptGenerator(Request $request){
        //product name, price, quantity, location
        $rawContent = $request->getContent();
        $this->isJSON($rawContent);
        
        $items = json_decode($rawContent, true);

        if(empty($items)){
            return $this->apiResponse('REQUEST__CAN_NOT_BE_EMPTY');
        }

        $taxModel = new Tax;

        $response = [];
        $subTotal = 0;
        $totalTax = 0;
        $total = 0;
        
        foreach($items as $item){
            if(empty($item)){
                return $this->apiResponse('ITEM__CAN_NOT_BE_EMPTY');                
            }
            if(
                !isset($item['productName'])   || $item['productName'] == ""
                || !isset($item['price'])       || $item['price'] == ""
                || !isset($item['quantity'])    || $item['quantity'] == ""
                || !isset($item['location'])    || $item['location'] == ""
            ){
                return $this->apiResponse('ITEM__MISSING_INFORMATIONS');
            }

            $tax_rate = $taxModel->getTaxRateByProductNameAndLocations($item['productName'], $item['location']);
            $sales_tax = $taxModel->calculateSalesTax($item['price'], $item['quantity'], $tax_rate);

            $subTotal += $item['price'] * $item['quantity'];
            $totalTax += $sales_tax;

            $response["items"][] = [
                "item" => $item['productName'],
                "price" => $item['price'],
            ];
        }
        $total = $subTotal + $totalTax;

        $response['subTotal'] = number_format($subTotal, 2);
        $response['tax'] = number_format($totalTax, 2);
        $response['total'] = number_format($total, 2);

        return $this->apiResponse('SUCCESS', $response);

    }
}
