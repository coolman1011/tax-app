<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Exceptions\ApiException;

class Tax extends Model
{
    private $taxRate = [
        'CA' => [
            'rate' => 0.0975,
            'exempt' => ["food"]
        ],
        'NY' => [
            'rate' => 0.0875,
            'exempt' => ["food","cloth"]
        ],
        'HK' => [
            'rate' => 0.02125,
            'exempt' => ["cloth"]
        ],
    ];

    private $productCategory = [
        'food' => [
            "apple", "banana", "potato chips"
        ],
        'cloth' => [
            "suit", "shirt"
        ]
    ];


    public function getTaxRateByProductNameAndLocations($productName, $location){
        $tax_rate = 0;

        if(!in_array($location, array_keys($this->taxRate))){
            throw new ApiException('ITEM__LOCATION_NOT_FOUND');
        }

        $productCateogry = "";
        foreach($this->productCategory as $category => $item){
            if(in_array($productName, $item)){
                $productCateogry = $category;
                break;
            }
        }
        
        $tax_rate = 0;
        
        if(isset($this->taxRate[$location])){
            $targetLocation = $this->taxRate[$location];
            
            if(!in_array($productCateogry, $targetLocation["exempt"])){
                $tax_rate = $targetLocation["rate"];
            }
        }

        return $tax_rate;
    }

    /**
     * Calculate sales tax, it will return 0 if sales tax rate is 0
     */
    public function calculateSalesTax($price, $quantity, $sales_tax){
        if($price <= 0){
            throw new ApiException('ITEM__PRICE_CAN_NOT_BE_ZERO');
        }

        if($quantity <= 0){
            throw new ApiException('ITEM__QUANTITY_CAN_NOT_BE_ZERO');
        }

        if($sales_tax == 0){
            return 0;
        }

        $tax = $price * $quantity * $sales_tax;
        if($tax > 0){
            $tax = $this->taxRoundUp($tax);
        }

        return $tax;
    }

    public function taxRoundUp($price){
        return (round(($price + 0.025) / 0.05, 0)) * 0.05;
    }
}
